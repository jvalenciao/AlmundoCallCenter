package com.callcenter.test.business;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.callcenter.business.Dispatcher;
import com.callcenter.model.Call;
import com.callcenter.queue.QueueCall;
import com.callcenter.util.Constant;
import com.callcenter.util.ConstantMessages;

public class DispacherTest {
	
	private final static Logger LOGGER = Logger.getLogger(Dispatcher.class);

	private Dispatcher dispatcher;

	@Before
	public void before() {
	}

	/**
	 * 
	 * Method for validate the creation list employees.
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gmail.com
	 */
	@Test
	public void loadEmployees() {
		
		LOGGER.info(ConstantMessages.DISPATCHERT_TEST_LOAD_EMPLOYEES.getMessage());

		dispatcher = new Dispatcher(Constant.SEVENT.getNumber(), Constant.THREE.getNumber(), Constant.ONE.getNumber());
		assertEquals(dispatcher.getFreeEmployeessOperator().size(), Constant.SEVENT.getNumber());
		assertEquals(dispatcher.getFreeEmployeessSupervisor().size(), Constant.THREE.getNumber());
		assertEquals(dispatcher.getFreeEmployeessDirector().size(), Constant.ONE.getNumber());
	}

	/**
	 * 
	 * Method for validation dispatch employee operator
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gmail.com
	 */
	@Test
	public void dispatchCallOperator() {

		LOGGER.info(ConstantMessages.DISPATCHERT_TEST_DISPATCH_CALL_OPERATOR.getMessage());
		
		dispatcher = new Dispatcher(Constant.SEVENT.getNumber(), Constant.TWO.getNumber(), Constant.ONE.getNumber());
		Call call = new Call();
		call.setId(System.currentTimeMillis());
		getClass();

		dispatcher.dispatchCall(call, dispatcher.getFreeEmployeessOperator().remove(Constant.ZERO.getNumber()));
		int sizeListEployeeOperator = dispatcher.getFreeEmployeessOperator().size();

		try {
			Thread.sleep(Constant.TEN_THOUSAND.getNumber());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// se valida que al momento de ejecutar el metodo dispatchCall solo se
		// disminuye la lista de operadores disponible, y qeu al finalizar el
		// hilo se vuelve a tener el emleado disponible
		assertEquals(sizeListEployeeOperator, Constant.SIX.getNumber());
		assertEquals(dispatcher.getFreeEmployeessSupervisor().size(), Constant.TWO.getNumber());
		assertEquals(dispatcher.getFreeEmployeessDirector().size(), Constant.ONE.getNumber());
		assertEquals(dispatcher.getFreeEmployeessOperator().size(), Constant.SEVENT.getNumber());

	}

	/**
	 * 
	 * Method for validation dispatch call supervisor
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gmail.com
	 */
	@Test
	public void dispatchCallSupervisor() {
		
		LOGGER.info(ConstantMessages.DISPATCHERT_TEST_DISPATCH_CALL_SUPERVISOR.getMessage());
		

		dispatcher = new Dispatcher(Constant.TWO.getNumber(), Constant.ONE.getNumber(), Constant.ONE.getNumber());

		// creationCalls
		Call call1 = new Call();
		call1.setId(System.currentTimeMillis());

		Call call2 = new Call();
		call2.setId(System.currentTimeMillis());

		Call call3 = new Call();
		call3.setId(System.currentTimeMillis());

		dispatcher.dispatchCall(call1, dispatcher.getFreeEmployeessOperator().remove(Constant.ZERO.getNumber()));

		dispatcher.dispatchCall(call2, dispatcher.getFreeEmployeessOperator().remove(Constant.ZERO.getNumber()));

		dispatcher.dispatchCall(call3, dispatcher.getFreeEmployeessSupervisor().remove(Constant.ZERO.getNumber()));

		int sizeListEployeeOperator = dispatcher.getFreeEmployeessOperator().size();
		int sizeListEployeeSupervisor = dispatcher.getFreeEmployeessSupervisor().size();

		try {
			Thread.sleep(Constant.FIFTEEN_THOUSAND.getNumber());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// se valida que al momento de ejecutar el metodo dispatchCall quede en
		// cero la lista de operadores y de supervisores, y al temrinar los
		// hilos
		// las listas que queden con todos los usuarios disponibles
		assertEquals(sizeListEployeeOperator, Constant.ZERO.getNumber());
		assertEquals(sizeListEployeeSupervisor, Constant.ZERO.getNumber());
		assertEquals(dispatcher.getFreeEmployeessDirector().size(), Constant.ONE.getNumber());
		assertEquals(dispatcher.getFreeEmployeessOperator().size(), Constant.TWO.getNumber());
		assertEquals(dispatcher.getFreeEmployeessSupervisor().size(), Constant.ONE.getNumber());

	}

	/**
	 * 
	 * Method for validation dispatch call director
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gmail.com
	 */
	@Test
	public void dispatchCallDirector() {

		LOGGER.info(ConstantMessages.DISPATCHERT_TEST_DISPATCH_CALL_DIRECTOR.getMessage());
		
		dispatcher = new Dispatcher(Constant.TWO.getNumber(), Constant.ONE.getNumber(), Constant.ONE.getNumber());

		// creationCalls
		Call call1 = new Call();
		call1.setId(System.currentTimeMillis());

		Call call2 = new Call();
		call2.setId(System.currentTimeMillis());

		Call call3 = new Call();
		call3.setId(System.currentTimeMillis());

		Call call4 = new Call();
		call4.setId(System.currentTimeMillis());

		// Dispathcall
		dispatcher.dispatchCall(call1, dispatcher.getFreeEmployeessOperator().remove(Constant.ZERO.getNumber()));

		dispatcher.dispatchCall(call2, dispatcher.getFreeEmployeessOperator().remove(Constant.ZERO.getNumber()));

		dispatcher.dispatchCall(call3, dispatcher.getFreeEmployeessSupervisor().remove(Constant.ZERO.getNumber()));

		dispatcher.dispatchCall(call4, dispatcher.getFreeEmployeessDirector().remove(Constant.ZERO.getNumber()));

		// Size list employess
		int sizeListEployeeOperator = dispatcher.getFreeEmployeessOperator().size();
		int sizeListEployeeSupervisor = dispatcher.getFreeEmployeessSupervisor().size();
		int sizeListEployeeDirector = dispatcher.getFreeEmployeessDirector().size();

		// Time sleep for threads
		try {
			Thread.sleep(Constant.FIFTEEN_THOUSAND.getNumber());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Validatios

		// se valida que al momento de ejecutar el metodo dispatchCall quede en
		// cero la lista de operadores y de supervisores, y al temrinar los
		// hilos
		// las listas que queden con todos los usuarios disponibles
		assertEquals(sizeListEployeeOperator, Constant.ZERO.getNumber());
		assertEquals(sizeListEployeeSupervisor, Constant.ZERO.getNumber());
		assertEquals(sizeListEployeeDirector, Constant.ZERO.getNumber());
		assertEquals(dispatcher.getFreeEmployeessDirector().size(), Constant.ONE.getNumber());
		assertEquals(dispatcher.getFreeEmployeessOperator().size(), Constant.TWO.getNumber());
		assertEquals(dispatcher.getFreeEmployeessSupervisor().size(), Constant.ONE.getNumber());

	}

	/**
	 * 
	 * Method execute class Dispatcher, this read from queue the list call next
	 * execute class processCall, that process 10 calls
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gmail.com
	 */
	@Test
	public void runTenCalls() {
		
		LOGGER.info(ConstantMessages.DISPATCHERT_TEST_RUN_TEN_CALLS.getMessage());

		// validation whit 10 employees and 10 calls
		dispatcher = new Dispatcher(Constant.SEVENT.getNumber(), Constant.THREE.getNumber(), Constant.ONE.getNumber());

		// creation calls
		Call call1 = new Call();
		Call call2 = new Call();
		Call call3 = new Call();
		Call call4 = new Call();
		Call call5 = new Call();
		Call call6 = new Call();
		Call call7 = new Call();
		Call call8 = new Call();
		Call call9 = new Call();
		Call call10 = new Call();

		call1.setId(1);
		call2.setId(2);
		call3.setId(3);
		call4.setId(4);
		call5.setId(5);
		call6.setId(6);
		call7.setId(7);
		call8.setId(8);
		call9.setId(9);
		call10.setId(10);

		// Queue Calls
		QueueCall queueCall = QueueCall.getInstance();
		queueCall.addCallQueue(call1);
		queueCall.addCallQueue(call2);
		queueCall.addCallQueue(call3);
		queueCall.addCallQueue(call4);
		queueCall.addCallQueue(call5);
		queueCall.addCallQueue(call6);
		queueCall.addCallQueue(call7);
		queueCall.addCallQueue(call8);
		queueCall.addCallQueue(call9);
		queueCall.addCallQueue(call10);

		Dispatcher dispatcher = new Dispatcher(Constant.SEVENT.getNumber(), Constant.TWO.getNumber(),
				Constant.ONE.getNumber());
		dispatcher.setProcessCall(Constant.TWENTY.getNumber());
		dispatcher.run();

		// Time sleep for threads
		try {
			Thread.sleep(Constant.TWENTY_THOUSAND.getNumber());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertEquals(dispatcher.getFreeEmployeessOperator().size(), Constant.SEVENT.getNumber());
		assertEquals(dispatcher.getFreeEmployeessSupervisor().size(), Constant.TWO.getNumber());
		assertEquals(dispatcher.getFreeEmployeessDirector().size(), Constant.ONE.getNumber());
	}
	
	
	/**
	 * 
	 * Method execute class Dispatcher, this read from queue the list call next
	 * execute class processCall, that process 20 calls	
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gmail.com
	 */
	@Test
	public void runTwentyCalls() {
		
		LOGGER.info(ConstantMessages.DISPATCHERT_TEST_RUN_TWENTY_CALLS.getMessage());

		// validation whit 10 employees and 10 calls
		dispatcher = new Dispatcher(Constant.SEVENT.getNumber(), Constant.THREE.getNumber(), Constant.ONE.getNumber());

		// creation calls
		Call call1 = new Call();
		Call call2 = new Call();
		Call call3 = new Call();
		Call call4 = new Call();
		Call call5 = new Call();
		Call call6 = new Call();
		Call call7 = new Call();
		Call call8 = new Call();
		Call call9 = new Call();
		Call call10 = new Call();
		Call call11 = new Call();
		Call call12 = new Call();
		Call call13 = new Call();
		Call call14 = new Call();
		Call call15 = new Call();
		Call call16 = new Call();
		Call call17 = new Call();
		Call call18 = new Call();
		Call call19 = new Call();
		Call call20 = new Call();

		call1.setId(1);
		call2.setId(2);
		call3.setId(3);
		call4.setId(4);
		call5.setId(5);
		call6.setId(6);
		call7.setId(7);
		call8.setId(8);
		call9.setId(9);
		call10.setId(10);
		call11.setId(10);
		call12.setId(10);
		call13.setId(10);
		call14.setId(10);
		call15.setId(10);
		call16.setId(10);
		call17.setId(10);
		call18.setId(10);
		call19.setId(10);
		call20.setId(10);

		// Queue Calls
		QueueCall queueCall = QueueCall.getInstance();
		queueCall.addCallQueue(call1);
		queueCall.addCallQueue(call2);
		queueCall.addCallQueue(call3);
		queueCall.addCallQueue(call4);
		queueCall.addCallQueue(call5);
		queueCall.addCallQueue(call6);
		queueCall.addCallQueue(call7);
		queueCall.addCallQueue(call8);
		queueCall.addCallQueue(call9);
		queueCall.addCallQueue(call10);
		queueCall.addCallQueue(call11);
		queueCall.addCallQueue(call12);
		queueCall.addCallQueue(call13);
		queueCall.addCallQueue(call14);
		queueCall.addCallQueue(call15);
		queueCall.addCallQueue(call16);
		queueCall.addCallQueue(call17);
		queueCall.addCallQueue(call18);
		queueCall.addCallQueue(call19);
		queueCall.addCallQueue(call20);

		Dispatcher dispatcher = new Dispatcher(Constant.SEVENT.getNumber(), Constant.TWO.getNumber(),
				Constant.ONE.getNumber());
		dispatcher.setProcessCall(Constant.THIRTY.getNumber());
		dispatcher.run();

		// Time sleep for threads
		try {
			Thread.sleep(Constant.THIRTY_THOUSAND.getNumber());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertEquals(dispatcher.getFreeEmployeessOperator().size(), Constant.SEVENT.getNumber());
		assertEquals(dispatcher.getFreeEmployeessSupervisor().size(), Constant.TWO.getNumber());
		assertEquals(dispatcher.getFreeEmployeessDirector().size(), Constant.ONE.getNumber());
	}

}
