package com.callcenter.model;

import java.io.Serializable;
import java.util.Date;

import com.callcenter.util.StatusCall;

/**
 * Class that represent a Call
 * @author avalencia
 *
 */
public class Call implements Serializable {
	
	/**
	 * serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id call
	 */
	private long id;
	
	/**
	 * stauts call
	 */
	private StatusCall statusCall;
	
	/**
	 * duration call
	 */
	private long durationCall;
	
	/**
	 * date call
	 */
	private Date dateCall;

	/**
	 * Method responsible for obtaining the value of id
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Method responsible for assigning the value of id
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Method responsible for obtaining the value of statusCall
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the statusCall
	 */
	public StatusCall getStatusCall() {
		return statusCall;
	}

	/**
	 * Method responsible for assigning the value of statusCall
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @param statusCall the statusCall to set
	 */
	public void setStatusCall(StatusCall statusCall) {
		this.statusCall = statusCall;
	}

	/**
	 * Method responsible for obtaining the value of durationCall
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the durationCall
	 */
	public long getDurationCall() {
		return durationCall;
	}

	/**
	 * Method responsible for assigning the value of durationCall
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @param durationCall the durationCall to set
	 */
	public void setDurationCall(long durationCall) {
		this.durationCall = durationCall;
	}

	/**
	 * Method responsible for obtaining the value of dateCall
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the dateCall
	 */
	public Date getDateCall() {
		return dateCall;
	}

	/**
	 * Method responsible for assigning the value of dateCall
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @param dateCall the dateCall to set
	 */
	public void setDateCall(Date dateCall) {
		this.dateCall = dateCall;
	}

}
