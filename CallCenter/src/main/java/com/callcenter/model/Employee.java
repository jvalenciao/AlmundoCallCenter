package com.callcenter.model;

import java.io.Serializable;
import java.util.Date;

import com.callcenter.util.Rol;
import com.callcenter.util.Status;

/**
 * Class that represent a employee
 * @author avalencia
 *
 */
public class Employee implements Serializable {
	
	/**
	 * Serial 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id employee
	 */
	private long id;
	
	/**
	 * Rol  employee
	 */
	private Rol rol;
	
	/**
	 * stuatus employee
	 */
	private Status status;
	
	/**
	 * name  employee
	 */
	private String name;
	
	/**
	 * last name employee
	 */
	private String lastName;
	
	/**
	 * emial employee
	 */
	private String email;
	
	/**
	 * birthday employee
	 */
	private Date birthday;

	/**
	 * Method responsible for obtaining the value of id
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Method responsible for assigning the value of id
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Method responsible for obtaining the value of rol
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the rol
	 */
	public Rol getRol() {
		return rol;
	}

	/**
	 * Method responsible for assigning the value of rol
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @param rol the rol to set
	 */
	public void setRol(Rol rol) {
		this.rol = rol;
	}

	/**
	 * Method responsible for obtaining the value of name
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Method responsible for assigning the value of name
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Method responsible for obtaining the value of lastName
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Method responsible for assigning the value of lastName
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Method responsible for obtaining the value of email
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Method responsible for assigning the value of email
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Method responsible for obtaining the value of birthday
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the birthday
	 */
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * Method responsible for obtaining the value of status
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * Method responsible for assigning the value of status
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * Method responsible for assigning the value of birthday
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @param birthday the birthday to set
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	
	
}
