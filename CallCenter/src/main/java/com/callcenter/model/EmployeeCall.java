package com.callcenter.model;

import java.io.Serializable;

/**
 * Class that represent employee for call
 * @author avalencia
 *
 */
public class EmployeeCall implements Serializable {
	
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * id hitorry call
	 */
	private long id;

	/**
	 * employee
	 */
	private Employee employee;

	/**
	 * call
	 */
	private Call call;

	/**
	 * Method responsible for obtaining the value of id
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Method responsible for assigning the value of id
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Method responsible for obtaining the value of employee
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * Method responsible for assigning the value of employee
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @param employee the employee to set
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * Method responsible for obtaining the value of call
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the call
	 */
	public Call getCall() {
		return call;
	}

	/**
	 * Method responsible for assigning the value of call
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @param call the call to set
	 */
	public void setCall(Call call) {
		this.call = call;
	}
	
	

}
