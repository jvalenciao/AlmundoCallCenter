package com.callcenter.util;

/**
 * Enum that conteind type rol employee
 * @author avalencia
 *
 */
public enum Rol {
	
	OPERATOR("OPERATOR","Rol operator"),
	SUPERVISOR("SUPERVISOR","Rol supervisor"),
	DIRECTOR("DIRECTOR","Rol director");
	
	/**
	 * Rol employee
	 */
	private String rol;
	
	/**
	 * Description rol employee
	 */
	private String description;


	/**
	 * Constructor of classe
	 * @param rol, rol employee
	 * @param description, description employee
	 */
	private Rol(String rol, String description) {
		this.rol = rol;
		this.description = description;
	}



	/**
	 * Method responsible for obtaining the value of rol
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the rol
	 */
	public String getRol() {
		return rol;
	}



	/**
	 * Method responsible for obtaining the value of description
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}




}
