package com.callcenter.util;

/**
 * Class util for constant number
 * @author avalencia
 *
 */
public enum Constant {
	
	
	ZERO(0),
	ONE(1),
	TWO(2),
	THREE(3),
	SIX(6),
	SEVENT(7),
	TWENTY(20),
	THIRTY(30),
	HUNDRED(100),
	TWO_HUNDRED(100),
	THREE_HUNDRED(300),
	ONE_THOUSAND(1000),
	THREE_THOUSAND(5000),
	FIVE_THOUSAND(5000),
	TEN_THOUSAND(10000),
	FIFTEEN_THOUSAND(15000),
	TWENTY_THOUSAND(20000),
	THIRTY_THOUSAND(30000);
	
	private int number;
	
	Constant(int number) {
		this.number = number;
	}

	/**
	 * Method responsible for obtaining the value of number
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}



}
