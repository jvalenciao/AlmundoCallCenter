package com.callcenter.util;

/**
 * Enum for status call
 * @author avalencia
 *
 */
public enum StatusCall {

	PROGRESS("PROGRESS"," status call in progress"),
	FINISHED("FINISHED"," status finish"),
	ONHOLD("ONHOLD"," status on hold");
	
	/**
	 * status call
	 */
	private String statusCall;
	
	/**
	 * description status call
	 */
	private String description;
	

	/**
	 * Method constructor status call
	 * @param statusCall, stautus call
	 * @param description, description
	 */
	private StatusCall(String statusCall, String description) {
		this.statusCall = statusCall;
		this.description = description;
	}

	/**
	 * Method responsible for obtaining the value of statusCall
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the statusCall
	 */
	public String getStatusCall() {
		return statusCall;
	}

	/**
	 * Method responsible for obtaining the value of description
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	
}
