package com.callcenter.util;

/**
 * Class util for constant messages
 * @author avalencia
 *
 */
public enum ConstantMessages {
	
	//messages logger
	START_METHOD("Start method "),
	FINISH_METHOD("Finish method "),
	METHOD_RUN("run: "),
	METHOD_FIND_EMPLOYEE("findEmployee: "),
	METHOD_DISPATCH_CALL("dispatchCall"),
	METHOD_UPDATE("update"),
	POINT("."),
	CALL_ID(" CallId: "),
	EMPLOYEE_ID(" EmployeeId: "),
	PROCESS_CALL_ID(" ProcessCallId: "),
	SIZE_LIST_EMPLOYEES("List employees: "),
	LIST_OPERATOR(" List Opertor: "),
	LIST_SUPERVISOR(" List Supervisor: "),
	LIST_DIRECTO(" List Director: "),
	ALL_EMPLOYESS_BUSSY("All the employees are busy, we will attend to you in a moment"),
	NOT_QUEUE_CALLS("There are no queued calls"),
	UPDATE_EMPLOYEE("Availible employee: "),
	TIME_CALL(" Time Call: "),
	DISPATCHERT_TEST_LOAD_EMPLOYEES("======================= DispatcherTest.loadEmployees ==========================="),
	DISPATCHERT_TEST_DISPATCH_CALL_OPERATOR("======================= DispatcherTest.dispatchCallOperator ==========================="),
	DISPATCHERT_TEST_DISPATCH_CALL_SUPERVISOR("======================= DispatcherTest.dispatchCallSupervisor ==========================="),
	DISPATCHERT_TEST_DISPATCH_CALL_DIRECTOR("======================= DispatcherTest.dispatchCallDirector ==========================="),
	DISPATCHERT_TEST_RUN_TEN_CALLS("======================= DispatcherTest.runTenCalls ==========================="),
	DISPATCHERT_TEST_RUN_TWENTY_CALLS("======================= DispatcherTest.runTwentyCalls ==========================="),
	
	
	//messages general
	OPERATOR("operator"),
	SUPERVISOR("supervisor"),
	DIRECTOR("director"),
	EXT_EMAIL("@almundo.com")
	;
	
	private String message;
	
	ConstantMessages(String message) {
		this.message = message;
	}

	/**
	 * Method responsible for obtaining the value of message
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}




}
