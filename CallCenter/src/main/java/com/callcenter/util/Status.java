package com.callcenter.util;

/**
 * Enum of status eployee
 * @author avalencia
 *
 */
public enum Status {
	
	AVAILIBLE("AVAILIBLE"," status availble"),
	BUSY("BUSY"," status bysy");
	
	/**
	 * status employee
	 */
	private String status;
	
	/**
	 * description status employee
	 */
	private String description;

	/**
	 * Constructor status employee
	 * @param status, status employee
	 * @param description, description status employee
	 */
	private Status(String status, String description) {
		this.status = status;
		this.description = description;
	}

	/**
	 * Method responsible for obtaining the value of status
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Method responsible for obtaining the value of description
	 * @autor Alexander Valencia </br>
	 * 			alexandervalenciao@gamail.com
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	

}
