package com.callcenter.business;

import java.io.Serializable;
import java.util.Date;
import java.util.Observable;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.log4j.Logger;

import com.callcenter.model.EmployeeCall;
import com.callcenter.util.Constant;
import com.callcenter.util.ConstantMessages;
import com.callcenter.util.Status;

/**
 * Calss for process calls
 * @author avalencia
 *
 */
public class ProcessCall extends Observable implements Serializable, Runnable {

	/**
	 * Consant for logger
	 */
	private final static Logger LOGGER = Logger.getLogger(ProcessCall.class);
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * id hitorry call
	 */
	private long id;

	/**
	 * employee
	 */
	private EmployeeCall employeeCall;

	@Override
	public void run() {

		LOGGER.info(new StringBuilder().append(ConstantMessages.START_METHOD.getMessage())
				.append(ProcessCall.class.getSimpleName()).append(ConstantMessages.POINT.getMessage())
				.append(ConstantMessages.METHOD_RUN.getMessage()).append(ConstantMessages.EMPLOYEE_ID.getMessage())
				.append(employeeCall.getEmployee().getId()).append(ConstantMessages.CALL_ID.getMessage())
				.append(employeeCall.getCall().getId()).append(ConstantMessages.PROCESS_CALL_ID.getMessage())
				.append(id));

		try {
			// set status busy to employee
			employeeCall.getEmployee().setStatus(Status.BUSY);

			// get time random between 5000 and 10000
			double timeRandom = ThreadLocalRandom.current().nextInt(Constant.FIVE_THOUSAND.getNumber(),
					Constant.TEN_THOUSAND.getNumber());

			// calculate time of call
			long timeCall = Math.round(timeRandom / Constant.ONE_THOUSAND.getNumber());

			// Time sleep thread while the call is answerered
			Thread.sleep(timeCall);

			employeeCall.getCall().setDurationCall(timeCall);
			employeeCall.getCall().setDateCall(new Date());
			employeeCall.getEmployee().setStatus(Status.AVAILIBLE);
			setChanged();
			notifyObservers(employeeCall.getEmployee());

			LOGGER.info(new StringBuilder().append(ConstantMessages.FINISH_METHOD.getMessage())
					.append(ProcessCall.class.getSimpleName()).append(ConstantMessages.POINT.getMessage())
					.append(ConstantMessages.METHOD_RUN.getMessage()).append(ConstantMessages.EMPLOYEE_ID.getMessage())
					.append(employeeCall.getEmployee().getId()).append(ConstantMessages.CALL_ID.getMessage())
					.append(employeeCall.getCall().getId()).append(ConstantMessages.PROCESS_CALL_ID.getMessage())
					.append(id).append(ConstantMessages.TIME_CALL.getMessage()).append(timeCall));

		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Method responsible for assigning the value of id
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gamail.com
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Method responsible for assigning the value of employeeCall
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gamail.com
	 * @param employeeCall
	 *            the employeeCall to set
	 */
	public void setEmployeeCall(EmployeeCall employeeCall) {
		this.employeeCall = employeeCall;
	}

}
