package com.callcenter.business;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Observable;
import java.util.Observer;

import org.apache.log4j.Logger;

import com.callcenter.model.Call;
import com.callcenter.model.Employee;
import com.callcenter.model.EmployeeCall;
import com.callcenter.queue.QueueCall;
import com.callcenter.util.Constant;
import com.callcenter.util.ConstantMessages;
import com.callcenter.util.Rol;

/**
 * Class for dispatcher calls	
 * @author avalencia
 *
 */
public class Dispatcher implements Runnable, Observer {

	/**
	 * Consant for logger
	 */
	private final static Logger LOGGER = Logger.getLogger(Dispatcher.class);

	/**
	 * list employees oeperators
	 */
	private List<Employee> freeEmployeessOperator;

	/**
	 * List employess supervisor
	 */
	private List<Employee> freeEmployeessSupervisor;

	/**
	 * list employees director
	 */
	private List<Employee> freeEmployeessDirector;

	/**
	 * Number of call to process
	 */
	private int processCall;

	/**
	 * @param freeEmployeess
	 */
	public Dispatcher(int totalEmployeesOperator, int totalEmployeesSupervisor, int totalEmployeesDirector) {

		loadEmployeesFree(totalEmployeesOperator, totalEmployeesSupervisor, totalEmployeesDirector);
	}

	/**
	 * Method run, where execute thread
	 */
	@Override
	public void run() {

		LOGGER.info(new StringBuilder().append(ConstantMessages.START_METHOD.getMessage())
				.append(Dispatcher.class.getSimpleName()).append(ConstantMessages.POINT.getMessage())
				.append(ConstantMessages.METHOD_RUN.getMessage()));

		while (processCall > Constant.ZERO.getNumber()) {

			try {

				try {

					QueueCall.getInstance().validateCallQueue();
					Employee employeeFree = finEmployee();

					if (employeeFree != null) {
						dispatchCall(QueueCall.getInstance().getCallQueue(), employeeFree);

					} else {

						LOGGER.info(new StringBuilder().append(ConstantMessages.ALL_EMPLOYESS_BUSSY.getMessage()));
						Thread.sleep(Constant.THREE_THOUSAND.getNumber());
					}

				} catch (NoSuchElementException e) {
					LOGGER.info(new StringBuilder().append(ConstantMessages.NOT_QUEUE_CALLS.getMessage()));
					Thread.sleep(Constant.THREE_THOUSAND.getNumber());
				}

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}

			processCall--;
		}

	}

	/**
	 * 
	 * Method that dispatch call in, and assings to employee
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gmail.com
	 * @param call,
	 *            call in
	 */
	public synchronized void dispatchCall(Call call, Employee employee) {

		LOGGER.info(new StringBuilder().append(ConstantMessages.START_METHOD.getMessage())
				.append(Dispatcher.class.getSimpleName()).append(ConstantMessages.POINT.getMessage())
				.append(ConstantMessages.METHOD_DISPATCH_CALL.getMessage())
				.append(ConstantMessages.CALL_ID.getMessage()).append(call.getId())
				.append(ConstantMessages.EMPLOYEE_ID.getMessage()).append(employee.getId()));

		ProcessCall processCall = new ProcessCall();
		EmployeeCall employeeCall = new EmployeeCall();

		employeeCall.setCall(call);
		employeeCall.setEmployee(employee);
		employeeCall.setId(System.currentTimeMillis());
		processCall.setEmployeeCall(employeeCall);
		processCall.setId(System.currentTimeMillis());

		processCall.addObserver(this);

		Thread thread = new Thread(processCall);
		thread.start();

		LOGGER.info(new StringBuilder().append(ConstantMessages.FINISH_METHOD.getMessage())
				.append(Dispatcher.class.getSimpleName()).append(ConstantMessages.POINT.getMessage())
				.append(ConstantMessages.METHOD_DISPATCH_CALL.getMessage())
				.append(ConstantMessages.CALL_ID.getMessage()).append(call.getId())
				.append(ConstantMessages.EMPLOYEE_ID.getMessage()).append(employee.getId()));

	}

	/**
	 * 
	 * Method that find employee free
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gmail.com
	 * @return employee free
	 */
	private synchronized Employee finEmployee() {

		LOGGER.info(new StringBuilder().append(ConstantMessages.START_METHOD.getMessage())
				.append(Dispatcher.class.getSimpleName()).append(ConstantMessages.POINT.getMessage())
				.append(ConstantMessages.METHOD_FIND_EMPLOYEE.getMessage()));

		Employee employeeFind = new Employee();

		if (freeEmployeessOperator.size() > Constant.ZERO.getNumber()) {
			employeeFind = freeEmployeessOperator.remove(Constant.ZERO.getNumber());

		} else if (freeEmployeessSupervisor.size() > Constant.ZERO.getNumber()) {
			employeeFind = freeEmployeessSupervisor.remove(Constant.ZERO.getNumber());

		} else if (freeEmployeessDirector.size() > Constant.ZERO.getNumber()) {
			employeeFind = freeEmployeessDirector.remove(Constant.ZERO.getNumber());

		} else {
			employeeFind = null;
		}

		LOGGER.info(new StringBuilder().append(ConstantMessages.SIZE_LIST_EMPLOYEES.getMessage())
				.append(ConstantMessages.LIST_OPERATOR.getMessage()).append(freeEmployeessOperator.size())
				.append(ConstantMessages.LIST_SUPERVISOR.getMessage()).append(freeEmployeessSupervisor.size())
				.append(ConstantMessages.LIST_DIRECTO.getMessage()).append(freeEmployeessDirector.size()));

		LOGGER.info(new StringBuilder().append(ConstantMessages.FINISH_METHOD.getMessage())
				.append(Dispatcher.class.getSimpleName()).append(ConstantMessages.POINT.getMessage())
				.append(ConstantMessages.METHOD_FIND_EMPLOYEE.getMessage())
				.append(ConstantMessages.EMPLOYEE_ID.getMessage())
				.append(employeeFind != null ? employeeFind.getId() : null));

		return employeeFind;
	}

	/**
	 * Method to receive notificacion of employee free and assings employee to
	 * list free employees
	 */
	@Override
	public synchronized void update(Observable o, Object arg) {

		LOGGER.info(new StringBuilder().append(ConstantMessages.METHOD_UPDATE.getMessage())
				.append(Dispatcher.class.getSimpleName()).append(ConstantMessages.POINT.getMessage())
				.append(ConstantMessages.METHOD_FIND_EMPLOYEE.getMessage()));

		Employee employee = (Employee) arg;

		LOGGER.info(new StringBuilder().append(ConstantMessages.UPDATE_EMPLOYEE.getMessage()).append(employee.getId()));

		if (employee.getRol().getRol().equals(Rol.OPERATOR.getRol())) {
			freeEmployeessOperator.add(employee);

		} else if (employee.getRol().getRol().equals(Rol.SUPERVISOR.getRol())) {
			freeEmployeessSupervisor.add(employee);

		} else if (employee.getRol().getRol().equals(Rol.DIRECTOR.getRol())) {
			freeEmployeessDirector.add(employee);

		}

		LOGGER.info(new StringBuilder().append(ConstantMessages.FINISH_METHOD.getMessage())
				.append(Dispatcher.class.getSimpleName()).append(ConstantMessages.POINT.getMessage())
				.append(ConstantMessages.METHOD_UPDATE.getMessage()));

	}

	/**
	 * 
	 * Method for generar data of de employees
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gmail.com
	 * @param totalEmployeesOperator,
	 *            total employees operator
	 * @param totalEmployeesSupervisor,
	 *            totmal employess supervisor
	 * @param totalEmployeesDirector,
	 *            total employees director
	 */
	private void loadEmployeesFree(int totalEmployeesOperator, int totalEmployeesSupervisor,
			int totalEmployeesDirector) {

		freeEmployeessOperator = new ArrayList<>();
		freeEmployeessSupervisor = new ArrayList<>();
		freeEmployeessDirector = new ArrayList<>();

		// creation employees operator
		for (int i = 0; i < totalEmployeesOperator; i++) {

			Employee employee = new Employee();
			employee.setId(Constant.HUNDRED.getNumber() + i);
			employee.setRol(Rol.OPERATOR);
			employee.setEmail(new StringBuilder(ConstantMessages.OPERATOR.getMessage()).append(i)
					.append(ConstantMessages.EXT_EMAIL.getMessage()).toString());
			employee.setName(new StringBuilder(ConstantMessages.OPERATOR.getMessage()).append(i).toString());
			freeEmployeessOperator.add(employee);
		}

		// creationemployees supervisor
		for (int i = 0; i < totalEmployeesSupervisor; i++) {

			Employee employee = new Employee();
			employee.setId(Constant.TWO_HUNDRED.getNumber() + i);
			employee.setRol(Rol.SUPERVISOR);
			employee.setEmail(new StringBuilder(ConstantMessages.SUPERVISOR.getMessage()).append(i)
					.append(ConstantMessages.EXT_EMAIL.getMessage()).toString());
			employee.setName(new StringBuilder(ConstantMessages.SUPERVISOR.getMessage()).append(i).toString());
			freeEmployeessSupervisor.add(employee);
		}

		// creation employee director
		for (int i = 0; i < totalEmployeesDirector; i++) {

			Employee employee = new Employee();
			employee.setId(Constant.THREE_HUNDRED.getNumber() + i);
			employee.setRol(Rol.DIRECTOR);
			employee.setEmail(new StringBuilder(ConstantMessages.DIRECTOR.getMessage()).append(i)
					.append(ConstantMessages.EXT_EMAIL.getMessage()).toString());
			employee.setName(new StringBuilder(ConstantMessages.DIRECTOR.getMessage()).append(i).toString());
			freeEmployeessDirector.add(employee);
		}

	}

	/**
	 * Method responsible for obtaining the value of freeEmployeessOperator
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gamail.com
	 * @return the freeEmployeessOperator
	 */
	public List<Employee> getFreeEmployeessOperator() {
		return freeEmployeessOperator;
	}

	/**
	 * Method responsible for obtaining the value of freeEmployeessSupervisor
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gamail.com
	 * @return the freeEmployeessSupervisor
	 */
	public List<Employee> getFreeEmployeessSupervisor() {
		return freeEmployeessSupervisor;
	}

	/**
	 * Method responsible for obtaining the value of freeEmployeessDirector
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gamail.com
	 * @return the freeEmployeessDirector
	 */
	public List<Employee> getFreeEmployeessDirector() {
		return freeEmployeessDirector;
	}

	/**
	 * Method responsible for obtaining the value of processCall
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gamail.com
	 * @return the processCall
	 */
	public int getProcessCall() {
		return processCall;
	}

	/**
	 * Method responsible for assigning the value of processCall
	 * 
	 * @autor Alexander Valencia </br>
	 *        alexandervalenciao@gamail.com
	 * @param processCall
	 *            the processCall to set
	 */
	public void setProcessCall(int processCall) {
		this.processCall = processCall;
	}

}
