package com.callcenter.queue;

import java.util.LinkedList;
import java.util.Queue;

import com.callcenter.model.Call;

/**
 * Class responsability of queue calls
 * @author avalencia
 *
 */
public class QueueCall {
	
	/**
	 * Queue of calls
	 */
	private Queue<Call> queueCall;
	
	/**
	 * instance of queue call
	 */
	private static QueueCall instance;
	
	private QueueCall() {
		
		queueCall = new LinkedList<Call>();
				
	}
	
	/**
	 * Method for set call in queue
	 * @param call, call to set queue
	 */
	public synchronized void addCallQueue(Call call) {
		getInstance().queueCall.add(call);
	}
	
	/**
	 * Method for get call in queue
	 * @return
	 */
	public synchronized Call getCallQueue() {
		return getInstance().queueCall.poll();
	}
	
	/**
	 * Method for get call in queue
	 * @return
	 */
	public synchronized Call validateCallQueue() {
		return getInstance().queueCall.element();
	}
	
	/**
	 * Get instance class, whit pattern singleton
	 * @return instance class QueueCall
	 */
	public static QueueCall getInstance() {
		
		if (instance == null) {
			instance = new QueueCall();
		}
		
		return instance;
		
	}


	

}
